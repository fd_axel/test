import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import "./header.css"
import logo from "../images/logo.svg"

const Header = ({ siteTitle }) => (
  <header>
    <div className="navbar">
      <Link classname="image" to="/"><img src={logo} /></Link>
      <div className="container">
        <div className="img">
        <Link className="navbar-item" to="/page-2">Use cases</Link>
        <Link className="navbar-item" to="/page-2">Products</Link>
        <Link className="navbar-item" to="/page-2">Price</Link>
        <Link className="navbar-item" to="/page-2">Documentation</Link>
        <Link className="navbar-item" to="/page-2">API</Link>
        </div>
      </div>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
